package com.qadomi.omar.mawdoo3;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.qadomi.omar.mawdoo3.Login.LoginInteractorImpl;
import com.qadomi.omar.mawdoo3.Login.LoginPresenterImpl;
import com.qadomi.omar.mawdoo3.Login.LoginView;

import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(MockitoJUnitRunner.class)
public class LoginActivityTest {
    @Mock
    LoginView view;
    @Mock
    LoginInteractorImpl interactor;
    @Mock
    private LoginPresenterImpl presenter;

    @Before
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.qadomi.omar.mawdoo3", appContext.getPackageName());

        presenter=new LoginPresenterImpl(view,new LoginInteractorImpl(appContext));
    }
    @Test
    public void shouldShowUsernameErrorWhenEmpty()
    {
        presenter.login("","123");
        verify(view).setEmailError();
        verify(view).hideProgress();
    }
    @Test
    public void shouldShowPasswordErrorWhenEmpty()
    {
        presenter.login("omar@yahoo.com","");
        verify(view).showProgress();
        verify(view).setPasswordError();
        verify(view).hideProgress();
    }

}