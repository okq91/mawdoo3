package com.qadomi.omar.mawdoo3.Realm;

import io.realm.RealmObject;

/**
 * Created by qadomio on 12/18/2017.
 */

public class ImageModel extends RealmObject {
    private int id;
    private String name;
    private String path;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
