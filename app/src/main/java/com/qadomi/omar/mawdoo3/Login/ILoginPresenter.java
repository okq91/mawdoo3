package com.qadomi.omar.mawdoo3.Login;

/**
 * Created by qadomio on 12/18/2017.
 */

public interface ILoginPresenter {
    void registerUser(String username, String password);
    void login(String username, String password);
    void onStart();
}
