package com.qadomi.omar.mawdoo3;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

/**
 * Created by qadomio on 12/18/2017.
 */

public class ShowImageDialog extends DialogFragment {
    ImageView imageView;
    String imagePath;
    Button btnOk;

    public static ShowImageDialog newInstance(String path) {
        ShowImageDialog frag = new ShowImageDialog();
        frag.imagePath = path;
        return frag;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_image, container, false);
        imageView = rootView.findViewById(R.id.img_view);
        btnOk = rootView.findViewById(R.id.btn_ok);
        Glide.with(getActivity())
                .load(imagePath)
                .placeholder(R.drawable.loading_icon)
                .error(R.drawable.photo)

                .into(imageView);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        return rootView;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        super.show(manager, tag);
    }
}
