package com.qadomi.omar.mawdoo3.Main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.qadomi.omar.mawdoo3.Adapters.ImagesAdapter;
import com.qadomi.omar.mawdoo3.Login.LoginActivity;
import com.qadomi.omar.mawdoo3.R;
import com.qadomi.omar.mawdoo3.Realm.ImageModel;
import com.qadomi.omar.mawdoo3.ShowImageDialog;

import java.util.List;

public class MainActivity extends AppCompatActivity implements MainView, ImagesAdapter.ItemClickListener {
    RecyclerView recyclerView;
    private IMainPresenter presenter;
    ProgressDialog progressDialog;
    List<ImageModel> imageList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.images_recycler);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(getString(R.string.Main));
        presenter = new MainPresenterImpl(this, new MainInteractorImpl(this));
        presenter.onCreate();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_sign_out) {
            presenter.onSignOutClicked();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null)
            progressDialog.hide();
    }

    @Override
    public void setImages(List<ImageModel> images) {
        imageList = images;
        ImagesAdapter adapter = new ImagesAdapter(this, images);
        adapter.setClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void showMessage(String message) {
        Snackbar.make(recyclerView,message,Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void goToLoginScreen() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public void onItemClick(View view, int position) {
        presenter.sendNotification(imageList.get(position).getName());
        ShowImageDialog showImageDialog = ShowImageDialog.newInstance(imageList.get(position).getPath());
        showImageDialog.show(getFragmentManager(), "");
    }
}
