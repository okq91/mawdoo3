package com.qadomi.omar.mawdoo3.FCM;

import android.app.Notification;
import android.app.NotificationManager;
import android.media.RingtoneManager;
import android.net.Uri;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.qadomi.omar.mawdoo3.R;

import java.util.Random;

/**
 * Created by qadomio on 12/19/2017.
 */

public class FCMMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //show notification
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Notification n  = new Notification.Builder(this.getApplicationContext())
                .setContentTitle(remoteMessage.getNotification().getBody())
                .setContentText("Image clicked")
                .setSound(defaultSoundUri)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setAutoCancel(false).build();


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        notificationManager.notify(new Random().nextInt(), n);
        super.onMessageReceived(remoteMessage);
    }
}
