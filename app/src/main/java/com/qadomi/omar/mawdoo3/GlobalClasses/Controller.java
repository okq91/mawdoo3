package com.qadomi.omar.mawdoo3.GlobalClasses;

import android.app.Application;

import com.google.firebase.FirebaseApp;

import io.realm.Realm;

/**
 * Created by qadomio on 12/18/2017.
 */

public class Controller extends Application {

    private static Controller controller;
    public Controller()
    {
    }
    @Override
    public void onCreate() {
        FirebaseApp.initializeApp(this);
        Realm.init(this);
        controller=this;
        super.onCreate();
    }
    public static Controller getInstance()
    {
        return controller;
    }
}
