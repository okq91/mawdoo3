package com.qadomi.omar.mawdoo3.Realm;

import io.realm.Realm;
import io.realm.RealmResults;


public class RealmController {

    private static RealmController instance;
    protected final Realm realm;

    public RealmController() {
        realm = Realm.getDefaultInstance();
    }

    public static RealmController getInstance() {
        if (instance == null) {
            instance = new RealmController();
        }
        return instance;
    }

    public Realm getRealm() {

        return realm;
    }

    //Refresh the realm istance
    public void refresh() {

        realm.refresh();
    }

    public RealmResults<ImageModel> getImages() {
        return realm.where(ImageModel.class).findAll();
    }

    public void deleteAll() {
        realm.where(ImageModel.class).findAll().deleteAllFromRealm();

    }

    public boolean hasImages() {

        return !realm.where(ImageModel.class).findAll().isEmpty();
    }

    public void saveImageObj(ImageModel imageModel) {
        realm.beginTransaction();
        realm.copyToRealm(imageModel);
        realm.commitTransaction();

    }
}