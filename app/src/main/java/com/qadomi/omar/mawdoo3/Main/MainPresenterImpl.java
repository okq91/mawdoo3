package com.qadomi.omar.mawdoo3.Main;

import com.qadomi.omar.mawdoo3.Realm.ImageModel;

import java.util.List;

/**
 * Created by qadomio on 12/18/2017.
 */

public class MainPresenterImpl implements IMainPresenter, IMainInteractor.OnFinishedListener {

    private MainView mainView;
    private IMainInteractor mainViewInteractor;

    public MainPresenterImpl(MainView mainView, MainInteractorImpl mainViewInteractor) {
        this.mainView = mainView;
        this.mainViewInteractor = mainViewInteractor;
    }

    @Override
    public void onCreate() {
        if (mainView != null) {
            mainView.showProgress();
        }
        //get images list from server or realm database if exists
        mainViewInteractor.getImages(this);
    }

    @Override
    public void onItemClicked(int position) {
        if (mainView != null) {
            mainView.showMessage(String.format("Position %d clicked", position + 1));
        }
    }

    @Override
    public void onSignOutClicked() {
        mainViewInteractor.signOut(this);
    }

    @Override
    public void onDestroy() {
        mainView = null;
    }

    @Override
    public void sendNotification(String imageName) {
        mainViewInteractor.sendNotification(imageName);
    }


    @Override
    public void onFinished(List<ImageModel> imageModelList) {
        if (mainView != null && imageModelList != null) {
            mainView.setImages(imageModelList);
            mainView.hideProgress();
        }
    }

    @Override
    public void onNetworkError() {
        mainView.showMessage("Network Error");
    }

    @Override
    public void onSignedOut() {
        mainView.goToLoginScreen();
    }
}