package com.qadomi.omar.mawdoo3.Main;

import android.content.Context;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;
import com.qadomi.omar.mawdoo3.API.ApiClient;
import com.qadomi.omar.mawdoo3.API.ApiInterface;
import com.qadomi.omar.mawdoo3.GlobalClasses.Controller;
import com.qadomi.omar.mawdoo3.GlobalClasses.SharedFunctions;
import com.qadomi.omar.mawdoo3.R;
import com.qadomi.omar.mawdoo3.Realm.ImageModel;
import com.qadomi.omar.mawdoo3.Realm.RealmController;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by qadomio on 12/18/2017.
 */

public class MainInteractorImpl implements IMainInteractor {
    Context context;
    public MainInteractorImpl(Context context) {
        this.context=context;
    }

    @Override
    public void sendNotification(String imageName) {
        if (SharedFunctions.isNetworkAvailable(context)) {
            String serverKey =context.getString(R.string.fcm_server_access_key);
            JSONObject json = new JSONObject();
            JSONObject notificationJson = new JSONObject();

            try {
                notificationJson.put("body", imageName);
                notificationJson.put("title", "dummy title");
                json.put("notification", notificationJson);
                json.put("to", FirebaseInstanceId.getInstance().getToken());

            } catch (JSONException e) {
                e.printStackTrace();
            }
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), String.valueOf(json));
            ApiInterface apiService =
                    ApiClient.getFCMClient().create(ApiInterface.class);
            Call<Void> call = apiService.sendNotification(serverKey, body);
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {

                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {

                }
            });
        }
    }

    private static String getAccessToken() throws IOException {
        FirebaseApp.getInstance().getToken(true);
        GoogleCredential googleCredential = GoogleCredential
                .fromStream(new FileInputStream("mawdoo3-service-account.json"));
        //.createScoped(CloudNaturalLanguageScopes.all());
        googleCredential.refreshToken();
        return googleCredential.getAccessToken();
    }

    @Override
    public void getImages(final OnFinishedListener listener) {
        if (RealmController.getInstance().hasImages()) {
            List<ImageModel> imageModelList = RealmController.getInstance().getImages();
            listener.onFinished(imageModelList);
        } else {
            if (SharedFunctions.isNetworkAvailable(context)) {
                ApiInterface apiService =
                        ApiClient.getClient().create(ApiInterface.class);
                Call<List<ImageModel>> call = apiService.getAllImages();
                call.enqueue(new Callback<List<ImageModel>>() {
                    @Override
                    public void onResponse(Call<List<ImageModel>> call, Response<List<ImageModel>> response) {
                        RealmController.getInstance().deleteAll();
                        for (ImageModel image : response.body()
                                ) {
                            RealmController.getInstance().saveImageObj(image);
                        }
                        listener.onFinished(response.body());
                    }

                    @Override
                    public void onFailure(Call<List<ImageModel>> call, Throwable t) {
                        listener.onFinished(null);

                    }
                });
            } else {
                listener.onNetworkError();
            }
        }
    }

    @Override
    public void signOut(OnFinishedListener listener) {
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        mAuth.signOut();
        listener.onSignedOut();
    }


}
