package com.qadomi.omar.mawdoo3.Main;

/**
 * Created by qadomio on 12/18/2017.
 */

public interface IMainPresenter {
    void onCreate();
    void onItemClicked(int position);
    void onSignOutClicked();
    void onDestroy();
    void sendNotification(String imageName);
}
