package com.qadomi.omar.mawdoo3.API;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
 
    public static final String BASE_URL = "http://mawdoo3tasks-env.x7jpm5jqzc.eu-west-1.elasticbeanstalk.com/";
    //public static final String FCM_URL = "https://fcm.googleapis.com/v1/projects/mawdoo3-934ad/messages:send";
      public static final String FCM_URL = "https://fcm.googleapis.com/fcm/";
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        return retrofit;
    }
    public static Retrofit getFCMClient() {
            retrofit = new Retrofit.Builder()
                    .baseUrl(FCM_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

        return retrofit;
    }
}