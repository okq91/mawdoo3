package com.qadomi.omar.mawdoo3.Login;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.qadomi.omar.mawdoo3.GlobalClasses.SharedFunctions;

import static android.content.ContentValues.TAG;

/**
 * Created by qadomio on 12/18/2017.
 */

public class LoginInteractorImpl implements ILoginInteractor {
    Context context;

    public LoginInteractorImpl(Context context) {
        this.context = context;
    }

    @Override
    public void register(final String email, final String password, final OnLoginFinishedListener listener) {

        if (TextUtils.isEmpty(email)) {
            listener.onUsernameError();
            return;
        }
        if (TextUtils.isEmpty(password)) {
            listener.onPasswordError();
            return;
        }
        if (SharedFunctions.isNetworkAvailable(context)) {
            final FirebaseAuth mAuth = FirebaseAuth.getInstance();
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success,
                                Log.d(TAG, "createUserWithEmail:success");
                                FirebaseUser user = mAuth.getCurrentUser();
                                listener.onSuccess();
                            } else {
                                // If register in fails, display a message to the user.
                                Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                if (task.getException() != null) {
                                    listener.onRegisterError(task.getException().getMessage());
                                    return;
                                }
                                listener.onFailure();
                            }


                        }
                    });
        } else {
            listener.onNetworkError();
        }

    }

    @Override
    public void login(String email, String password, final OnLoginFinishedListener listener) {
        if (TextUtils.isEmpty(email)) {
            listener.onUsernameError();
            return;
        }
        if (TextUtils.isEmpty(password)) {
            listener.onPasswordError();
            return;
        }
        if (SharedFunctions.isNetworkAvailable(context)) {
            final FirebaseAuth mAuth = FirebaseAuth.getInstance();
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success,
                                Log.d(TAG, "createUserWithEmail:success");
                                FirebaseUser user = mAuth.getCurrentUser();
                                listener.onSuccess();
                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                listener.onFailure();
                            }


                        }
                    });
        } else {
            listener.onNetworkError();
        }

    }
}