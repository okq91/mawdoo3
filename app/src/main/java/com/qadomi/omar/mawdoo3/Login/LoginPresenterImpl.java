package com.qadomi.omar.mawdoo3.Login;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created by qadomio on 12/18/2017.
 */

public class LoginPresenterImpl implements ILoginPresenter, ILoginInteractor.OnLoginFinishedListener {

    private LoginView loginView;
    private ILoginInteractor loginInteractor;

    public LoginPresenterImpl(LoginView loginView,LoginInteractorImpl loginInteractor) {
        this.loginView = loginView;
        this.loginInteractor = loginInteractor;
    }

    @Override public void registerUser(String username, String password) {
        if (loginView != null) {
            loginView.showProgress();
        }

        loginInteractor.register(username, password, this);
    }

    @Override
    public void login(String username, String password) {
        if (loginView != null) {
            loginView.showProgress();
        }

        loginInteractor.login(username, password, this);
    }

    @Override public void onStart() {
        // Check if user is signed in (non-null) and go to main screen directly
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null)
            loginView.navigateToHome();
    }

    @Override public void onUsernameError() {
        if (loginView != null) {
            loginView.setEmailError();
            loginView.hideProgress();
        }
    }

    @Override public void onPasswordError() {
        if (loginView != null) {
            loginView.setPasswordError();
            loginView.hideProgress();
        }
    }

    @Override public void onSuccess() {
        if (loginView != null) {
            loginView.navigateToHome();
        }
    }

    @Override
    public void onFailure() {
        loginView.hideProgress();
        loginView.setError("Wrong username or password.");

    }

    @Override
    public void onRegisterError(String message) {
        loginView.hideProgress();
        loginView.setError(message);
    }

    @Override
    public void onNetworkError() {
        loginView.hideProgress();
        loginView.setError("No internet connection");
    }
}