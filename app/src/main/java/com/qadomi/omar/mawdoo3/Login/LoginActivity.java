package com.qadomi.omar.mawdoo3.Login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.qadomi.omar.mawdoo3.Main.MainActivity;
import com.qadomi.omar.mawdoo3.R;

/**
 * A login screen that offers register or login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener, LoginView {


    // UI references.
    private EditText txtEmail;
    private EditText txtPassword;
    private Button btnSignIn;
    private Button btnSignUp;
    TextView tvError;
    private ILoginPresenter presenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the register form.
        txtEmail = findViewById(R.id.email);
        txtPassword = findViewById(R.id.password);
        btnSignUp = findViewById(R.id.btn_sign_up);
        btnSignUp.setOnClickListener(this);
        btnSignIn = findViewById(R.id.btn_sign_in);
        btnSignIn.setOnClickListener(this);
        tvError = findViewById(R.id.tv_error);
        presenter = new LoginPresenterImpl(this, new LoginInteractorImpl(this));
    }

    @Override
    public void onStart() {
        presenter.onStart();
        super.onStart();

    }

    //call presenter validation and register
    @Override
    public void onClick(View view) {
        tvError.setText("");
        if (view.getId() == R.id.btn_sign_up)
            presenter.registerUser(txtEmail.getText().toString(), txtPassword.getText().toString());
        else if (view.getId() == R.id.btn_sign_in)
            presenter.login(txtEmail.getText().toString(), txtPassword.getText().toString());
    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null)
            progressDialog.hide();
    }

    @Override
    public void setEmailError() {
        txtEmail.setError(getString(R.string.invalid_email));
    }

    @Override
    public void setPasswordError() {
        txtPassword.setError(getString(R.string.invalid_password));
    }

    @Override
    public void navigateToHome() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public String getError() {
        return tvError.getText().toString();
    }

    @Override
    public void setError(String message) {
        tvError.setText(message);
    }
}

