package com.qadomi.omar.mawdoo3.Main;

import com.qadomi.omar.mawdoo3.Realm.ImageModel;

import java.util.List;

/**
 * Created by qadomio on 12/18/2017.
 */

public interface IMainInteractor {
    interface OnFinishedListener {
        void onFinished(List<ImageModel> imageModelList);
        void onNetworkError();
        void onSignedOut();
    }
    void sendNotification(String imageName);
    void getImages(OnFinishedListener listener);
    void signOut(OnFinishedListener listener);
}
