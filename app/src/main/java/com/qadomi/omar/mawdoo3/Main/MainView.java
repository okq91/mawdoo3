package com.qadomi.omar.mawdoo3.Main;

import com.qadomi.omar.mawdoo3.Realm.ImageModel;

import java.util.List;

public interface MainView {

    void showProgress();

    void hideProgress();

    void setImages(List<ImageModel> images);

    void showMessage(String message);

    void goToLoginScreen();
}