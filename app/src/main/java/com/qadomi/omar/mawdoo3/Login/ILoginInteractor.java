package com.qadomi.omar.mawdoo3.Login;

/**
 * Created by qadomio on 12/18/2017.
 */

public interface ILoginInteractor {

    interface OnLoginFinishedListener {
        void onUsernameError();

        void onPasswordError();

        void onSuccess();

        void onFailure();

        void onRegisterError(String message);

        void onNetworkError();
    }

    void register(String username, String password, OnLoginFinishedListener listener);
    void login(String username, String password, OnLoginFinishedListener listener);


}