package com.qadomi.omar.mawdoo3.API;

import com.qadomi.omar.mawdoo3.Realm.ImageModel;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;


public interface ApiInterface {
    @GET("api/images")
    Call<List<ImageModel>> getAllImages();
    @POST("send")
    Call<Void> sendNotification(@Header("Authorization") String credentials, @Body RequestBody params);
 

}