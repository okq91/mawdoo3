package com.qadomi.omar.mawdoo3.Login;

/**
 * Created by qadomio on 12/18/2017.
 */
public interface LoginView {

    void showProgress();

    void hideProgress();

    void setEmailError();

    void setPasswordError();

    void navigateToHome();

    String getError();


    void setError(String message);
}